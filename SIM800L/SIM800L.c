/********************************************************
 * SIM800L.c
 *
 *  Created on: Nov 3, 2020
 * Author: Nathan Hanchey
 * Description: Controls the SIM800L Module
 *
 * References:
 * MSP432 Textbook by Mazidi and Naimi
 * SIM800L Datasheet & AT Commands
 ********************************************************/
#include "msp.h"
#include "string.h"
#include <stdio.h>
#include "SIM800L.h"

//Initialize the UART Port for the Module
void UART_init() {
    UART->CTLW0 |= 1; //reset mode
    UART->MCTLW = 0;
    UART->CTLW0 = 0x0081;
    UART->BRW = 312; //115200 = 260 //312 = 9600

    //Pin/Port Declaration as defined in the header
    UART_PORT->SEL0 |= 0x0C;
    UART_PORT->SEL1 &= ~0x0C;
    UART->CTLW0 &= ~1; // not reset mode

    //Interrupt
    UART->IE |= 1; // Interrupt Handler
    ENABLE
    __enable_irq();

    increment = 0;
}

//RX Handler for Receiving Data
//Comment out Handler you're not using
void EUSCIA2_IRQHandler() {

    array[increment] = UART->RXBUF;
    increment++;

}

//void EUSCIA1_IRQHandler() {
//
//    array[increment] = UART->RXBUF;
//    increment++;
//
//}

//Clears String of the Received Data
void clear_array() {

    int i;

    for(i = 0; 100 > i; i++) {
        array[i] = 0;
    }
    increment = 0;
}

//Begins a call with number input
void makeCall(char number[]) {
    char send[24];
    char Init_Call[] = "AT+CREG=1\r\n";

    sprintf(send, "ATD+ +1%s;\r\n", number);

    sendUART(Init_Call);
    delayms(100);
    sendUART(send);
    delayms(100);
}

//Sends Texts
//Input: Text to Send and Phone Number
void sendText(char message[], char number[]) {
    char send[50];
    char mess[100];
    char SUB[50];

    char Init_Text[] = "AT+CMGF=1\r\n";



    sprintf(send, "AT+CMGS=\"+1%s\"\r\n", number);
    sprintf(mess, "%s\r\n", message);
    sprintf(SUB, "%c\r\n", 0x1A);//0x1A


    sendUART(Init_Text);
    delayms(100);
    sendUART(send);
    delayms(100);
    sendUART(mess);
    delayms(100);
    sendUART(SUB);
    delayms(100);
}

//UART Command
//Input: UART Command (Carriage Return not included)
void sendUART(char message[]) {
    int i;

    for(i = 0; i < strlen(message); i++) {
        while(!(UART->IFG & 0x02));
        UART->TXBUF = message[i];
    }
}

void delayms(int n) {
    int i, j;

    for(j = 0; j < n; j++)
            for(i = 250; i > 0 ; i--);
}
