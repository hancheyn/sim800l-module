#include "msp.h"
#include "string.h"
#include <stdio.h>
#include "SIM800L.h"

/*********************************************************
 * Author: Nathan Hanchey
 * Description: Controls the SIM800L Module
 *
 * References:
 * MSP432 Textbook by Mazidi and Naimi
 * SIM800L Datasheet & AT Commands
 *
 ********************************************************/

void main(void) {

    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;     // stop watchdog timer

    //Variables & Strings
    char AT[] = "AT\r\n";
    char hangup[] = "ATH\r\n";
    char answer[] = "ATA\r\n";
    char mess[] = "Hello";
    int opt;

    delayms(4000);
    //Initialization for Module
    UART_init();
    delayms(4000);
    sendUART(AT);
    delayms(3000);
    printf("%s\n", array);
    increment = 0;
    clear_array();

    //MAIN LOOP
    while(1) {

        //Console Menu for testing the SIM800L
        printf("Enter 1 for Text | Enter 2 for Call | 3 for Command Send\n");
        scanf("%d", &opt);

        //Option 1 is Texting
        if(opt == 1) {
            char num[100];
            printf("Enter 10 digit number: \n");
            scanf("%s", &num);
            printf("Message Sending: Hello\n");

            sendText(mess, num); //Sends Hello as a test
            delayms(1000);
            printf("%s\n", array);
            delayms(1000);
            increment = 0;
            clear_array(); //Clears Array
        }
        //Option 2 is Calling
        else if (opt == 2) {
            char num[100];
            printf("Enter 10 digit number: \n");
            scanf("%s", &num);
            printf("Calling: \n");

            makeCall(num);
            delayms(1000);
            printf("%s\n", array);
            increment = 0;
            clear_array();

        }
        //Option 3 is Command with carriage return
        else if(opt == 3) {

            char num[100];
            char send[100];

            printf("Enter command:\n");
            scanf("%s", &num);
            printf("Message Sending:\n");

            sprintf(send, "%s\r\n", num);

            sendUART(send);
            delayms(2000);
            printf("%s\n", array);
            delayms(1000);
            increment = 0;
            clear_array();
        }
        else {
            printf("Try Again\n");
            delayms(1000);
            sendUART(AT);
            delayms(3000);
            printf("%s\n", array);
            increment = 0;
            //clear_array();
        }

    }

}

